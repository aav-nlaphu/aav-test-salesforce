public with sharing class CallWSPageController {
    public WSRequest__c req {get; set;}
    public WSResponse__c res {get; set;}
    
    public CallWSPageController() {
        req = new WSRequest__c();
        req.Username__c = 'test';
        req.Password__c = 'test';
    }
    
    public void callWS(){
        res = null;
        if (req.Endpoint__c == null || String.isEmpty(req.Endpoint__c)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Endpoint is empty!')); 
            return;
        }
        if (req.Method__c == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Please choose a method!'));
            return;
        }
        
        WSInvoker wsInvoker = new WSInvoker();
        res = wsInvoker.invoke(req.Username__c, req.Password__c, req.Endpoint__c, req.Method__c, req.Data__c);
    }
}