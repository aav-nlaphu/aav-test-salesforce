@isTest
public class WSInvokerTest {
	@isTest
    public static void callWS() {
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, new WSInvokerMock());
        WSInvoker wsInvoker = new WSInvoker();
        
        WSResponse__c response = wsInvoker.invoke('test', 'test', '', 'GET', '');
        
        Test.stopTest();
        
        System.assertNotEquals(null, response);
        System.assertEquals(200, response.Status_Code__c);
        System.assertEquals('OK', response.Status__c);
        System.assertEquals('{"code" : "OK", "data": "This is ws result"}', response.Body__c);
    }
}