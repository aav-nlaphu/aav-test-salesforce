@isTest
public class CallWSPageControllerTest {

    @isTest
    public static void testCallSuccess() {
        Test.setMock(HttpCalloutMock.class, new WSInvokerMock());
        Test.startTest();

        PageReference pageRef = Page.CallWSPage;
        Test.setCurrentPage(pageRef);

        CallWSPageController controller = new CallWSPageController();
        controller.req.Method__c = 'GET';
        controller.req.Endpoint__c = 'https://leaddriverapi.com/api/clients?_format=json';
        controller.req.Data__c = 'foo=bar';
        controller.callWS();

        Test.stopTest();

        System.assertNotEquals(null, controller.res);
        System.assertEquals(200, controller.res.Status_Code__c);
        System.assertEquals('OK', controller.res.Status__c);
    }

    @isTest
    public static void testEmptyEndpoint() {
        Test.setMock(HttpCalloutMock.class, new WSInvokerMock());
        Test.startTest();

        PageReference pageRef = Page.CallWSPage;
        Test.setCurrentPage(pageRef);

        CallWSPageController controller = new CallWSPageController();
        controller.req.Method__c = 'GET';
        controller.callWS();

        Test.stopTest();

        System.assertEquals(null, controller.res);
    }

    @isTest
    public static void testEmptyMethod() {
        Test.setMock(HttpCalloutMock.class, new WSInvokerMock());
        Test.startTest();

        PageReference pageRef = Page.CallWSPage;
        Test.setCurrentPage(pageRef);

        CallWSPageController controller = new CallWSPageController();
        controller.req.Endpoint__c = 'https://leaddriverapi.com/api/clients?_format=json';
        controller.callWS();

        Test.stopTest();

        System.assertEquals(null, controller.res);
    }
}