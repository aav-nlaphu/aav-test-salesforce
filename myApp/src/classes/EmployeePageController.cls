public with sharing class EmployeePageController {
	public Employee__c e { get; set; }

	public EmployeePageController() {
		e = new Employee__c();
	}

	public PageReference submitEmployee() {
		INSERT e;
		return new PageReference('/add_employee_success');
	}
}