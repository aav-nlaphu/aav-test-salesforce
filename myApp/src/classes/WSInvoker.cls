public class WSInvoker {
    
    public WSResponse__c invoke(String username, String password, String endPointURL, String method, String data) {
        Http h = new Http();
        
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endPointURL);
        request.setMethod(method);
        
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        request.setHeader('Authorization', authorizationHeader);
        request.setHeader('content-type', 'application/json; charset=utf-8');
        
        if (data != null && !String.isEmpty(data)) {
            request.setBody(data);
        }
        
        HttpResponse response = h.send(request);
        WSResponse__c wsResponse = new WSResponse__c();
        wsResponse.Status__c = response.getStatus();
        wsResponse.Status_Code__c = response.getStatusCode();
        wsResponse.Body__c = response.getBody();
        
        return wsResponse;
    }
}