global class WSInvokerMock implements HttpCalloutMock{
    global HttpResponse respond(HttpRequest request) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type' , 'application/json' );
        response.setBody('{"code" : "OK", "data": "This is ws result"}');
        response.setStatus('OK');
        response.setStatusCode(200);
        return response;
    }
}